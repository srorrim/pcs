<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    public function submit(Request $request) {
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required',

      ]); 

      // Create a new message
      $message = new Message;
      $message->name = $request->input('name');
      $message->email = $request->input('email');
      $message->message = $request->input('message');

      $message->save();

      return redirect('/#contact')->with('success', 'Thanks! We will get back to you right away!');


        $data = array('name'=>"$message->name", "body" => "$message->message");

        Mail::send('emails.mail', $data, function($message) {

        $message->to('macklangen.creative@gmail.com', 'Priority Creative Solutions')

                ->subject('PCS');

        $message->from('$message->email','$message->name');

  });
}
}
