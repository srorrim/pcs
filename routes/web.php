<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// the below code will put the construction page back on
// Route::group(['middleware' => 'under-construction'], function () {
//     Route::get('/', function() {
//         return view('main');
//     });
// });

// commented out to put the page under construction
Route::get('/', function () {
    return view('main');
});

Route::get('/about', function () {
  return view('about');
});

Route::get('/services', function () {
  return view('services');
});


Route::post('contact/submit', 'MessagesController@submit');
