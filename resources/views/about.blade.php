@extends('layouts.services-app')
@section('content')
@include('layouts.navbar2')
	<div class="page-section-about">

		<div class="about-hero">
			<div class="about-rose-container flex justify-center">

				<div class="flex justify-center about-rose fade-in one">
				</div>
			</div>
			<h2 class="fade-in two"><strong>WHERE CREATIVE CHAOS MEETS FUNCTION & FORM</strong></h2>
			<h3 class="fade-in two"><strong>Priority Creative Solutions is an agile and forward-thinking team of professionals.</strong><br>
				Focused on bringing new ideas to life with the latest and most powerful technologies the web has to offer.

			<br>We are committed to developing unique and effiecient ways to make our clients stand out from the rest.<br>
			With performance always being our priority, we make sure to always offer a lightweight and beautiful experience,<br> that is sure to help you reach your goals in style. </h3>
      <hr>
			<div class="arrow bounce">
			  <a class="fa fa-arrow-down fa-2x fade-in three" href="#process"></a>
			</div>
    </div>
</div>


<section class="page-section-about flex flex-vertical">
	<div class="about-process" id="process">

		<h1>Our Process</h1>
		<hr color="white" width="50%">
	</div>

	<div class="process-card-container flex justify-space-around">

		<div class="process-card">
			<div class="pc1"></div>
			<div class="process-card-description">
				<p class="flex justify-center">Planning & Analysis</p>
				<hr color="white" width="80%">
				<div class="flex justify-center">
					<div class="">
						<li>Purpose / Goals</li>
						<li>Technology</li>
						<li>Content</li>
						<li>Audience</li>
					</div>
				</div>
			</div>
		</div>

		<div class="process-card">
			<div class="pc2"></div>
			<div class="process-card-description">
				<p class="flex justify-center">Design & Development</p>
				<hr color="white" width="80%">
				<div class="flex justify-center">
					<div class="">
						<li>Resources</li>
						<li>Information Architechure</li>
						<li>Aesthetics / Design</li>
						<li>Code</li>
					</div>
				</div>

			</div>
		</div>

		<div class="process-card">
			<div class="pc3"></div>
			<div class="process-card-description">
				<p class="flex justify-center">Testing & Delivery</p>
				<hr color="white" width="80%">
				<div class="flex justify-center">
					<div class="">
							<li>Hosting / Domain Setup</li>
							<li>Configuration</li>
							<li>SEO / Performance</li>
							<li>Responsiveness / Mobile</li>
						</div>
					</div>
				</div>
			</div>

			<div class="process-card">
				<div class="pc4"></div>
				<div class="process-card-description">
					<p class="flex justify-center">Maintenance & Marketing</p>
					<hr color="white" width="80%">
					<div class="flex justify-center">
						<div class="">
							<li>Updates / Backups</li>
							<li>Content Writing</li>
							<li>Marketing / Adwords</li>
							<li>Security / I.T</li>
						</div>
					</div>
				</div>
			</div>

		</div>


	</section>

	<section class="page-section-about">
			<div class="profiles">
				<div class="us">
					<div class="flex justify-center"><div class="pp shawn"></div></div>
					<h1>Shawn</h1>
					<hr color="white" width="30%">
					<h3><strong>Director / Lead Developer</strong><br>
						croissant efficianado.<br>
						loves long walks on the beach.<br>
					</h3>
				</div>

				<div class="us">
					<div class="flex justify-center"><div class="pp mack"></div></div>
					<h1>Mack</h1>
					<hr color="white" width="30%">
					<h3><strong>Creative Director / Developer</strong><br>

						accomplished synchronized swimmer.<br>
						enjoys spanish cuisine.<br>
					</h3>
				</div>
			</div>
	</section>

@endsection
