@extends('layouts.services-app')
@section('content')
@include('layouts.navbar2')
<div id="site">
	<section class="page-section">
		<div id="development"></div>
		<img src="./images/one.svg" alt="" width="100%" height="100%">
		<div class="services-text">
			<h2>Web De<span id="accent">s</span>ign & Development</h2>
			<h3><strong>We are passionate about designing & developing unique and custom websites.</strong><br>
				Taking the time to understand your business,<br>
				and personalizing the experience with your goals in mind.
			</h3>
			<a href="/#contact" class="btn-services btn-nav btn-1 btn--cw"><code class="btn--inner"><h4>Get a Free Quote</h4></code></a>
		</div>
	</section>

	<section class="page-section">
		<div id="optimization"></div>
    <img src="./images/background1.svg" alt="" width="100%" height="100%">
		<div class="services-text">
			<h2>Optimization & <span id="accent">P</span>erforman<span id="accent">c</span>e</h2>
			<h3><strong>Do you have an existing website that is running slow?</strong><br>
				·Make your site responsive to fit all device sizes and browsers!<br>
				·Speed up your site, brush off that digital dust & eliminate unneccesary burden<br>
				·Faster load times make your site easier to find, and easier to use.<br>
			</h3>
			<a href="/#contact" class="btn-services btn-nav btn-1 btn--cw"><code class="btn--inner"><h4>Contact Us Today!</h4></code></a>
		</div>

	</section>

	<section class="page-section">
		<div id="solutions"></div>
		<img src="./images/background2.svg" alt="" width="100%" height="100%">
		<div class="services-text">
			<h2><span id="accent">C</span>reative <span id="accent">S</span>olutions</h2>
			<h3><strong>Allow us to work with you to create a solution that fits your projects needs.</strong><br>
				·Have a partially completed project and need a boost?<br>
				·Already have your designs finished and in need of some development work?<br>
				·Take your functioning project and making it look and feel how youve always imagined.<br>
				·Let us take some wieght off of your shoulders and offer a fresh perspective.<br>
			</h3>
			<a href="/#contact" class="btn-services btn-nav btn-1 btn--cw"><code class="btn--inner"><h4>Hire Us For Your Project</h4></code></a>
		</div>
	</section>
</div>

@endsection
