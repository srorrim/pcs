@extends('layouts.app')
@section('content')
{{-- ------------------------------------------------------ HERO  --}}
  <section class="intro">
    <div class="content">
      <div class="logo">
        <div class="header-logo"></div>
        <h1>Priority <span id="accent">Creative</span> Solutions.</h1>
      </div>

   </div>
  </section>
  @include('layouts.navbar')
{{-- ----------------------------------------------------- About Us --}}
  <section class="about">
    <div class="content">
      <div class="about-container">
        <div class="about-image fade-in one"></div>
        <div class="about-text">
          <h2 class="fade-in one"><strong>"Only those who attempt the absurd will achieve the impossible."</strong></h2>
          <h3 class="fade-in two">At Priority Creative Solutions, we love a challenge - <br>
             Our team has a passion for creating fast, engaging online experiences.<br>
              Each project tailored to our clients goals and unique sense of style. </h3>
              <a href="/about" class="btn-services btn-nav btn-1 btn--cw fade-in one"><code class="btn--inner"><h4>Learn more about us</h4></code></a>
              <div class="arrow bounce">
                <a class="fa fa-arrow-down fa-2x fade-in three" href="#services"></a>
              </div>
        </div>

      </div>
   </div>
  </section>
{{-- ----------------------------------------------------- Services  --}}
  <section class="services" id="services">
    <h1>Our Services</h1>
    <hr color="white" width="40%">
    <div id="app" class="card-container">
      <card data-image="https://s3.ca-central-1.amazonaws.com/pcs.img/sone.svg">

        <h1 slot="header"><a href="services#development">Web Design & Development</a></h1>
        <p slot="content"><a href="services#development">Let us take your idea from concept to creation - Get noticed, and grow your business online!</a></p>

      </card>
      <card data-image="https://s3.ca-central-1.amazonaws.com/pcs.img/sthree.svg" href="/#optimization">
        <h1 slot="header"><a href="services#optimization">Performance &<br> Optimization</a></h1>
        <p slot="content"><a href="services#optimization">Have an existing site that you would like to be re-vamped for todays online experience?</a></p>
      </card>

      <card data-image="https://s3.ca-central-1.amazonaws.com/pcs.img/stwo.svg" href="/#solutions">
        <h1 slot="header"><a href="services#solutions">Creative Solutions</a></h1>
        <p slot="content"><a href="services#solutions">Hit a speedbump in your current project? Let us take some of the wieght off of your shoulders!</a></p>
      </card>
    </div>

  </section>
{{-- ----------------------------------------------------- Contact  --}}
  <section class="contact" id="contact">
    <div class="content">
      <div class="h1-container">

        <h1>Contact</a></h1>
        <h3>Send us a message, we would love to work with you! <br>
        Or maybe you just have some questions?<br> We always provide free quotes and will get back to you right away!</h3>
      </div>
      <div class="contact-form-container">

        <div class="contact-form">
          <div class="contact-info-container">
            <div class="contact-info">
              <div class="contact-logo"></div>
              <h5 id="contact-call"><i class="fa fa-envelope" aria-hidden="true"></i></h5>
              <h3><a href="mailto:shawn@prioritycreativesolutions.com">shawn@prioritycreativesolutions.com</a><br>
            <a href="mailto:mack@prioritycreativesolutions.com">mack@prioritycreativesolutions.com</a></h3>

            <h5 id="contact-call"><i class="fa fa-phone" aria-hidden="true"></i></h5>
            <a href="tel:+4036076225">1-(403)-607-6225</a> <br></h3><a href="tel:+5877185557"> 1-(587)-718-5557</a></h3>
            </div>
          </div>

          {!! Form::open(['url' => 'contact/submit']) !!}
          <div class="form-group">
            {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter name']) }}
          </div>
          <div class="form-group">
            {{ Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'person@internet.com']) }}
          </div>
          <div class="form-group">
            {{ Form::textarea('message', '', ['class' => 'form-control', 'placeholder' => 'Enter message...']) }}
          </div>
          @include('layouts.messages')



          <div>
            {{ Form::submit('Send', ['class' => 'btn btn-outline-secondary', 'type' => 'button'] )}}
          </div>



          {!! Form::close() !!}


        </div>

      </div>

   </div>
  </section>
{{-- ----------------------------------------------------- Footer  --}}
  <footer>
    <p>Calgary, Alberta</p>
  </footer>



@endsection
