<div id="navbar2">
  <a href="/">
    <img src="./../img/logo.svg" alt="" border="0">
  </a>
  <a href="/about" class="btn-nav btn-4 btn--trbl"><code class="btn--inner">About <span id="accent">Us</span></code></a>
  <a href="/services" class="btn-nav btn-1 btn--cw"><code class="btn--inner">Services</code></a>
  <a href="/#contact" class="btn-nav btn-1 btn--cw"><code class="btn--inner">Contact</code></a>

</div>

{{-- ---------------------------------------------------------------------- MOBILE  --}}

<!-- Mobile Nav -->
<div class="topnav" id="myTopnav">
  <a href="/">
      <h2>Priority <span id="accent">Creative</span> Solutions</h2>
  </a>
  <a href="/about" class="btn-nav btn-4 btn--trbl"><code class="btn--inner">About <span id="accent">Us</span></code></a>
  <a href="/services" class="btn-nav btn-1 btn--cw"><code class="btn--inner">Services</code></a>
  <a href="/#contact" class="btn-nav btn-1 btn--cw"><code class="btn--inner">Contact</code></a>
  <a href="javascript:void(0);" style="font-size:25px;" class="icon" onclick="mobileFunction()">&#9776;</a>
</div>



<script>
function mobileFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>
