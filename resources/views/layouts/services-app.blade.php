<!DOCTYPE html>
<html class="html">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Priority Creative Solutions is a Premier Web Design and Development firm located in Calgary, Alberta. Flexible rates & Free Quotes! ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <title>Priority Creative Solutions.</title>
  </head>
  <body class="body">
    @yield('content')
    <!-- <script type="text/javascript" src="/js/app.js"></script> -->
  </body>
</html>
