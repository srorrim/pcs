
@if(count($errors) > 0)
  @foreach($errors->all() as $error)
      <div class='alert alert-danger' id='status'>
        {{$error}}
      </div>
    @endforeach
  @endif


    @if(session('success'))
      <div class='alert alert-warning' id='status'>
        {{session('success')}}
      </div>
    @endif
