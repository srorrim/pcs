
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
// --------------------------------------------------------- Sticky Navbar
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");

var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

// ----------------------------------------------------------------Mobile menu

function mobileMenu() {
  var x = document.getElementById("menu");
  x.classList.add("show-menu");
}
function navFunction() {
  if (window.pageYOffset >= sticky) {
    topnav.classList.add("sticky")
  } else {
    topnav.classList.remove("sticky");
  }
}
// ------------------------------------------------------------------ Service Cards
Vue.config.devtools = true;

Vue.component("card", {
  template: "\n    <div class=\"card-wrap\"\n      @mousemove=\"handleMouseMove\"\n      @mouseenter=\"handleMouseEnter\"\n      @mouseleave=\"handleMouseLeave\"\n      ref=\"card\">\n      <div class=\"card\"\n        :style=\"cardStyle\">\n        <div class=\"card-bg\" :style=\"[cardBgTransform, cardBgImage]\"></div>\n        <div class=\"card-info\">\n          <slot name=\"header\"></slot>\n          <slot name=\"content\"></slot>\n        </div>\n      </div>\n    </div>",
  mounted: function mounted() {
    this.width = this.$refs.card.offsetWidth;
    this.height = this.$refs.card.offsetHeight;
  },

  props: ["dataImage"],
  data: function data() {
    return {
      width: 0,
      height: 0,
      mouseX: 0,
      mouseY: 0,
      mouseLeaveDelay: null
    };
  },
  computed: {
    mousePX: function mousePX() {
      return this.mouseX / this.width;
    },
    mousePY: function mousePY() {
      return this.mouseY / this.height;
    },
    cardStyle: function cardStyle() {
      var rX = this.mousePX * 30;
      var rY = this.mousePY * -30;
      return {
        transform: "rotateY(" + rX + "deg) rotateX(" + rY + "deg)"
      };
    },
    cardBgTransform: function cardBgTransform() {
      var tX = this.mousePX * -40;
      var tY = this.mousePY * -40;
      return {
        transform: "translateX(" + tX + "px) translateY(" + tY + "px)"
      };
    },
    cardBgImage: function cardBgImage() {
      return {
        backgroundImage: "url(" + this.dataImage + ")"
      };
    }
  },
  methods: {
    handleMouseMove: function handleMouseMove(e) {
      this.mouseX = e.pageX - this.$refs.card.offsetLeft - this.width / 2;
      this.mouseY = e.pageY - this.$refs.card.offsetTop - this.height / 2;
    },
    handleMouseEnter: function handleMouseEnter() {
      clearTimeout(this.mouseLeaveDelay);
    },
    handleMouseLeave: function handleMouseLeave() {
      var _this = this;

      this.mouseLeaveDelay = setTimeout(function () {
        _this.mouseX = 0;
        _this.mouseY = 0;
      }, 1000);
    }
  }
});

var app = new Vue({
  el: "#app"
});
// -------------------------------------------------------- Service page sections
// -------------------------------------------------------- About section imagined
